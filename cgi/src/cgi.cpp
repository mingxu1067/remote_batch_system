#include "cgi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <assert.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

CGI::CGI() {
    for (int i=0; i<MAX_CONNECTION; i++) {
        clients[i] = new Client();
    }

    connection_number = 0;
}
CGI::~CGI() {}

void CGI::service() {
    printHeader();
    printBody();
    sentJob();
    printTail();
}

void CGI::printHeader() {
    char header[] =
            "Content-type: text/html\n\n"
            "<html>\n"
            "<head>\n"
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n"
            "<title>Network Programming Homework 3</title>\n"
            "<body bgcolor=#336699>\n"
            "<font face=\"Courier New\" size=2 color=#FFFF99>\n"
            "<table width=\"800\" border=\"1\">\n";
    printf("%s", header);
    fflush(stdout);
}

void CGI::printBody() {
    char* query_str = getenv("QUERY_STRING");

    char* token = strtok(query_str, "&");
    while ((token != NULL) && (connection_number < 5)) {
        if( strlen(token) <= PARA_START_INDEX ){
            break;
        }
        int index = (token[1] - '0') - 1;
        clients[index]->setIp(&token[PARA_START_INDEX]);

        token = strtok(NULL, "&");
        clients[index]->setPort(&token[PARA_START_INDEX]);

        token = strtok(NULL, "&");
        clients[index]->setFilename(&token[PARA_START_INDEX]);

        token = strtok( NULL , "&" );

        clients[index]->setRequested(true);
    }

    char table_body[] = 
            "<body bgcolor=#336699>"
            "<font face=\"Courier New\" size=2 color=#FFFF99>"
            "<table width=\"800\" border=\"1\">\n";

    printf("%s", table_body);
    printf( "<tr>\n" );
    for ( int i=0; i<MAX_CONNECTION; i++){
        if (clients[i]->getRequested()){
            printf("<td>%s</td>", clients[i]->getIp());
        }
    }
    printf( "</tr>\n" );

    printf( "<tr>\n" );
    for ( int i=0; i<MAX_CONNECTION; i++){
        if (clients[i]->getRequested()){
            printf("<td valign=\"top\" id=\"m%d\"></td>", i);
        }
    }
    printf("</tr>\n</table>\n");
    fflush(stdout);
}

void CGI::printTail() {
    char tail[] =
            "</font>\n"
            "</body>\n"
            "</html>\n";

    printf("%s", tail);
    fflush(stdout);
}

void CGI::sentJob() {
    fd_set read_fds, write_fds, r_set, w_set;

    int client_fd[MAX_CONNECTION];
    int client_status[MAX_CONNECTION];

    struct sockaddr_in client_addr[MAX_CONNECTION];
    FILE *file_fd[MAX_CONNECTION];

    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);
    FD_ZERO(&r_set);
    FD_ZERO(&w_set);

    connection_number = 0;
    for (int i=0; i<MAX_CONNECTION; i++) {
        if (clients[i]->getRequested()) {
            client_fd[i] = socket( AF_INET, SOCK_STREAM, 0 );
            memset(&client_addr[i], 0, sizeof(client_addr[i]));

            client_addr[i].sin_family = AF_INET;
            client_addr[i].sin_addr = *((in_addr*)(gethostbyname(clients[i]->getIp())->h_addr));
            client_addr[i].sin_port = htons(atoi(clients[i]->getPort()));

            int flags = fcntl(client_fd[i], F_GETFL, 0);
            fcntl(client_fd[i], F_SETFL, flags | O_NONBLOCK);

            int connect_status = connect(client_fd[i], (struct sockaddr *)&client_addr[i], sizeof(client_addr[i]));
            ++connection_number;

            FD_SET(client_fd[i], &r_set);
            file_fd[i] = fopen(clients[i]->getFilename(), "r");

            client_status[i] = STATUS_CONNECTING;
        } else {
            client_status[i] = STATUS_DONE;
        }
    }

    int nfds = 1024;
    while (connection_number > 0) {
        memcpy(&read_fds, &r_set, sizeof(read_fds));
        memcpy(&write_fds, &w_set, sizeof(write_fds));

        int select_status = select(nfds, &read_fds, &write_fds, (fd_set*)0, (struct timeval*)0);
        if ( select_status < 0 ){
            printf("Execute select() Failed\n");
            exit(1);
        }

        for (int i=0; i<MAX_CONNECTION; i++) {
            if (clients[i]->getRequested()) {
                switch (client_status[i]) {
                    case STATUS_CONNECTING:
                        if (FD_ISSET(client_fd[i], &read_fds)){
                            int opt_value;
                            socklen_t opt_length = sizeof(int);

                            if(getsockopt(client_fd[i], SOL_SOCKET, SO_ERROR, (char*)&opt_value, &opt_length) < 0 || opt_value != 0 ){
                                close(client_fd[i]);
                                FD_CLR(client_fd[i], &r_set);
                                FD_CLR(client_fd[i], &w_set);

                                client_status[i] = STATUS_DONE;
                                --connection_number;
                            } else {
                                client_status[i] = STATUS_READING;
                            }
                        }
                        break;

                    case STATUS_READING:
                        if (FD_ISSET(client_fd[i], &read_fds)){
                            char buffer[BUFFER_SIZE] = {};
                            int n = read(client_fd[i], buffer, BUFFER_SIZE);
                            if (n <= 0) {
                                close(client_fd[i]);
                                FD_CLR(client_fd[i], &r_set);
                                FD_CLR(client_fd[i], &w_set);

                                client_status[i] = STATUS_DONE;
                                --connection_number;

                                continue;
                            } else {
                                printf("<script>document.all[\'m%d\'].innerHTML += \"", i);
                                for (int buffer_i= 0; buffer_i<strlen(buffer); buffer_i++){
                                    printTransfer(buffer[buffer_i]);
                                }
                                printf( "\";</script>\n" );
                                fflush(stdout);

                                if (isGetPromt(n, buffer)) {
                                    client_status[i] = STATUS_WRITING;
                                    FD_SET(client_fd[i], &w_set);
                                    FD_CLR(client_fd[i], &r_set);
                                }
                            }
                        }
                        break;

                    case STATUS_WRITING:
                        if (FD_ISSET(client_fd[i], &write_fds)){
                            if (clients[i]->isWriteEnd()) {
                                char* client_write_buffer = clients[i]->getBuffer();
                                fgets(client_write_buffer, BUFFER_SIZE, file_fd[i]);
                                printf("<script>document.all[\'m%d\'].innerHTML += \"", i);
                                printf("<b>");
                                for (int buffer_i= 0; buffer_i<strlen(client_write_buffer); buffer_i++){
                                    printTransfer(client_write_buffer[buffer_i]);
                                }
                                printf("</b>");
                                printf( "\";</script>\n" );
                                fflush(stdout);

                                clients[i]->resetSentedCount();
                            }

                            int write_count = strlen(clients[i]->getBuffer()) - clients[i]->getSentedCount();
                            int count = write(client_fd[i], clients[i]->getCurrentWriteBufferPosition(), write_count);
                            clients[i]->addSentedCount(count);

                            if(clients[i]->isWriteEnd()){
                                client_status[i] = STATUS_READING;
                                FD_SET(client_fd[i], &r_set);
                                FD_CLR(client_fd[i], &w_set);
                            }
                        }

                        break;

                    default:
                        break;
                }
            }
        }
    }
}

void CGI::printTransfer(char c) {
    if (c == '"' ){
        printf( "&quot;" );
    }else if(c == '&' ){
        printf( "&amp;" );
    }else if(c == '<' ){
        printf( "&lt;" );
    }else if(c == '>' ){
        printf( "&gt;" );
    }else if(c == '\n' ){
        printf( "<br>" );
    }else if(c == ' ' ){
        printf( "&nbsp;" );
    }else if(c == '\r'){
        return;
    }else{
        printf("%c",c );
    }
}

bool CGI::isGetPromt(int len, char* str) {
    for(int i=0; i<len; i++){
        if(str[i] == '%'){
            return true;
        }
    }

    return false;
}