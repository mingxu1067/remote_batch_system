#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <signal.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 32768

#define PACKET_TYPE_GET "GET"

#define CGI_FILE_EXET ".cgi" 

typedef struct {
    char type[10];
    char file_path[100];
    char query_string[BUFFER_SIZE];
    char protocol[20];
} HttpPacket;

HttpPacket readHttpPacket() {
    char buffer[BUFFER_SIZE];
    HttpPacket packet;
    scanf("%s", buffer);
    if (strcmp (buffer, PACKET_TYPE_GET) == 0) {
        strncpy(packet.type, PACKET_TYPE_GET, 10);
    }

    scanf("%s", buffer);
    if (strlen(buffer) > 0) {
        char file_name[100];
        char query[BUFFER_SIZE];
        int index = 0;

        char* temp = file_name;
        for (int i=1; i<strlen(buffer); i++) {
            char c = buffer[i];

            if (c == '?') {
                index = 0;
                temp = query;
            } else {
                *(temp+index) = c;
                ++index;
            }

        }

        strncpy(packet.file_path, file_name, 100);
        strncpy(packet.query_string, query, BUFFER_SIZE);
    }

    scanf("%s", buffer);
    strncpy(packet.protocol, buffer, 20);

    return packet;
}

void handleHttpPacket(HttpPacket* packet) {
    printf("%s 200 OK\n", packet->protocol);
    if( strstr(packet->file_path, CGI_FILE_EXET) != NULL ){
        clearenv();
        setenv("QUERY_STRING", packet->query_string, 0);
        setenv("SCRIPT_NAME", packet->file_path, 0);
        setenv("REQUEST_METHOD", packet->type, 0);
        setenv("REMOTE_ADDR", "127.0.0.1", 0);
        setenv("REMOTE_HOST", "", 0);
        setenv("CONTENT_LENGTH", "", 0);
        setenv("AUTH_TYPE", "", 0);
        setenv("REMOTE_USER", "", 0);
        setenv("REMOTE_IDENT", "", 0);
        execl(packet->file_path, packet->file_path, NULL);
    } else {
        printf("Content-type: text/html\n\n");
        std::ifstream file_reader(packet->file_path);
        std::string buffer;
        while(getline(file_reader, buffer)){
            printf("%s",buffer.c_str());
        }
        file_reader.close();
    }
}

int main(int argc, char const *argv[])
{
    int port = 6999;

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if ((para.find("--port=") == 0) || (para.find("-p=") == 0)) {
            port = atoi(para.substr(para.find("=") + 1).c_str());
        }
    }

    int listen_fd;
    struct sockaddr_in listen_address;
    memset((char*)&listen_address, 0, sizeof(listen_address));

    listen_address.sin_family = AF_INET;
    listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
    listen_address.sin_port = htons(port);

    int opt_value = 1;
    setsockopt( listen_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt_value, sizeof(int));

    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Fail to create socket.\n");
    }

    if(bind(listen_fd, (struct sockaddr *) &listen_address, sizeof(listen_address)) == -1) {
        fprintf(stderr, "Fail to bind socket.\n");
        exit(1);
    }

    if(listen(listen_fd, 0) == -1) {
        fprintf(stderr, "Fail to listen socket.\n");
        exit(1);
    }

    while (true) {
        int client_fd = -1;
        int client_len = 0;

        struct sockaddr_in client_addr;
        if (client_fd = accept(listen_fd, (struct sockaddr*) &client_addr, (socklen_t*)&client_len)) {
            pid_t pid = fork();
            if (pid == 0) {
                printf("Create child to serve the client %d\n", getpid());

                dup2(client_fd, STDIN_FILENO);
                dup2(client_fd, STDOUT_FILENO);
                dup2(client_fd, STDERR_FILENO);

                close(listen_fd);

                HttpPacket packet = readHttpPacket();
                handleHttpPacket(&packet);

                close(client_fd);
                exit(0);

            } else {
                close(client_fd);
            }
        }
    }

    close(listen_fd);
    return 0;
}