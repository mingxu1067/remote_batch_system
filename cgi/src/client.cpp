#include "client.h"
#include <stdio.h>
#include <string.h>

Client::Client() {}
Client::~Client() {}

void Client::setIp(char* ip) {
    strncpy(this->host_ip, ip, INET_ADDRSTRLEN);
}
char* Client::getIp() {
    return host_ip;
}

void Client::setPort(char* port) {
    strncpy(this->host_port, port, 6);
}
char* Client::getPort() {
    return host_port;
}

void Client::setFilename(char* filename) {
    strncpy(this->host_batch_file, filename, 100);
}
char* Client::getFilename() {
    return host_batch_file;
}

void Client::setRequested(bool status) {
    requested = status;
}
bool Client::getRequested() {
    return requested;
}

int Client::getSentedCount() {
    return client_sented_count;
}
char* Client::getBuffer() {
    return client_write_buffer;
}

bool Client::isWriteEnd() {
    return client_sented_count == strlen(client_write_buffer);
}

void Client::resetSentedCount() {
    client_sented_count = 0;
}

void Client::addSentedCount(int count) {
    client_sented_count += count;
}

char* Client::getCurrentWriteBufferPosition() {
    return client_write_buffer + client_sented_count;
}

void Client::printInfo() {
    printf("Ip: %s\n", host_ip);
    printf("port: %s\n", host_port);
    printf("file: %s\n", host_batch_file);
    printf("requested: %d\n", requested);
}