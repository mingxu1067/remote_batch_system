#ifndef RBS_CGI_H_
#define RBS_CGI_H_

#include <netinet/in.h>
#include "client.h"

#define STATUS_CONNECTING 000
#define STATUS_READING 999
#define STATUS_WRITING 998
#define STATUS_DONE 997

#define MAX_CONNECTION 5
#define PARA_START_INDEX 3

// #define BUFFER_SIZE 32768

class CGI {
public:
    CGI();
    ~CGI();

    void service();
    
private:
    Client* clients[MAX_CONNECTION];

    int connection_number;

    void printHeader();
    void printBody();
    void printTail();

    void sentJob();

    void printTransfer(char c);
    bool isGetPromt(int len, char* str);
};

#endif